﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class shopeg : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["tendangnhap"] != null)
        {
            hpltendangnhap.Visible = true;
            hpltendangnhap.Text = "<font color=black>Chào, </font>" + Session["tendangnhap"].ToString();
            hpldangky.Visible = false;
            hpldangnhap.Visible = false;
            lbtthoat.Visible = true;
            lbphancach2.Visible = true;
        }
        else
        {
            hpltendangnhap.Visible = false;
        }

    }

    protected void lbtthoat_Click(object sender, EventArgs e)
    {
        Session["tendangnhap"] = null;
        Session["giohang"] = null;
        Response.Redirect("~/Default.aspx");
    }
}
