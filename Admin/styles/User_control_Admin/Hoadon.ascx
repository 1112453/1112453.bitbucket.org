﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Hoadon.ascx.cs" Inherits="Admin_User_control_Admin_Hoadon" %>
<style type="text/css">
    .hiddencol
    {
        display:none;
    }
    .viscol
    {
        display:block;
    }
</style>
<table cellpadding="10" cellspacing="10" style="width: 100%;">
    <tr>
        <td 
 
            
            style="text-align: center; font-weight: 700; font-size: x-large; color: #FFFFFF; background-color: #67AC00;">
            QUẢN LÝ ĐƠN ĐẶT HÀNG</td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="Panel1" runat="server" Visible="False">
                <table cellpadding="10" cellspacing="10" style="width: 100%;">
                    <tr>
                        <td style="font-weight: 700">
                            Tên khách hàng</td>
                        <td style="font-weight: 700; color: #FF0000">
                            <asp:Label ID="lbtenkhachhang" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Tên người nhận :</b></td>
                        <td>
                            <asp:TextBox ID="txtennguoinhan" runat="server" Height="30px" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Địa chỉ nhận :</b></td>
                        <td>
                            <asp:TextBox ID="txtdiachinhan" runat="server" Height="50px" Width="300px" 
                                TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Địa chỉ nhận :</b></td>
                        <td>
                            <asp:TextBox ID="txtdienthoainhan" runat="server" Height="30px" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Đã giao:</b></td>
                        <td>
                            <asp:CheckBox ID="chkdagiao" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <asp:Button ID="btncapnhat" runat="server" Height="30px" Text="Cập nhật " 
                                Width="120px" onclick="btncapnhat_Click" />
                            &nbsp;<asp:Button ID="btnhuy" runat="server" Height="30px" Text="Hủy" 
                                Width="120px" onclick="btnhuy_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                CellPadding="10" CellSpacing="1" GridLines="None" 
                onrowcommand="GridView1_RowCommand">
                <Columns>
                    <asp:BoundField DataField="sodh" HeaderText="Số đặt hàng">
                    <HeaderStyle Width="30px" ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ten_sp" HeaderText="Tên sản phẩm" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="soluong" HeaderText="Số lượng" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="dongia" HeaderText="Đơn giá" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Tên khách hàng" DataField="hoten" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ngaydathang" HeaderText="Ngày đặt hàng" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ngaygiaohang" HeaderText="Ngày giao hàng" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="hinhthucthanhtoan" HeaderText="HTTT" >
                    <HeaderStyle ForeColor="White" />
                    </asp:CheckBoxField>
                    <asp:CheckBoxField DataField="hinhthucgiaohang" HeaderText="HTGH" >
                    <HeaderStyle ForeColor="White" />
                    </asp:CheckBoxField>
                    <asp:BoundField DataField="trigia" HeaderText="Trị giá" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="dagiao" HeaderText="Đã giao" >
                    <HeaderStyle ForeColor="White" />
                    </asp:CheckBoxField>
                    <asp:HyperLinkField DataNavigateUrlFields="ma_kh" 
                        DataNavigateUrlFormatString="~/Admin/Chitietdonhang.aspx?ma_kh={0}" 
                        Text="Chi tiết">
                    <HeaderStyle ForeColor="White" />
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="tennguoinhan" ItemStyle-CssClass="hiddencol">
<ItemStyle CssClass="hiddencol"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="diachinhan" ItemStyle-CssClass="hiddencol">
<ItemStyle CssClass="hiddencol"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="dienthoainhan" ItemStyle-CssClass="hiddencol">
<ItemStyle CssClass="hiddencol"></ItemStyle>
                    </asp:BoundField>
                    <asp:ButtonField CommandName="nutsua" Text="Sửa">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    </asp:ButtonField>
                    <asp:ButtonField CommandName="nutxoa" Text="xóa" >
                    <HeaderStyle ForeColor="White" />
                    </asp:ButtonField>
                </Columns>
                <HeaderStyle BackColor="#67AC00" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; color: #FFFFFF; font-weight: 700; font-size: x-large; background-color: #FF6600">
            <asp:Label ID="lbthongbaoloi" runat="server"></asp:Label>
        </td>
    </tr>
</table>

