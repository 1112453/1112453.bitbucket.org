﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Chitietdonhang.ascx.cs" Inherits="Admin_User_control_Admin_Chitietdonhang" %>
<p style="font-weight: 700; text-align: center; color: #FFFFFF; font-size: x-large; background-color: #489100;">
    XEM
    CHI TIẾT ĐƠN ĐẶT HÀNG</p>
<asp:DataList ID="DataList1" runat="server">
    <ItemTemplate>
        <asp:Panel ID="Panel1" runat="server">
            <table cellpadding="10" cellspacing="10" style="width:100%;" width="900px">
                <tr>
                    <td style="font-weight: 700">
                        Tên khách hàng :</td>
                    <td>
                        <b>
                        <asp:Label ID="hotenLabel0" runat="server" Text='<%# Bind("hoten") %>' />
                        </b>
                    </td>
                    <td>
                        <b>Hình thức thanh toán :</b></td>
                    <td>
                        <b>
                        <asp:CheckBox ID="hinhthucthanhtoanCheckBox1" runat="server" 
                            Checked='<%# Bind("hinhthucthanhtoan") %>' Enabled="false" />
                        </b>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        Số đặt hàng :</td>
                    <td>
                        <b>
                        <asp:Label ID="sodhLabel0" runat="server" Text='<%# Eval("sodh") %>' />
                        </b>
                    </td>
                    <td>
                        <b>Hình thức giao hàng </b>
                    </td>
                    <td>
                        <b>
                        <asp:CheckBox ID="hinhthucgiaohangCheckBox1" runat="server" 
                            Checked='<%# Bind("hinhthucgiaohang") %>' Enabled="false" />
                        </b>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        Ngày đặt hàng :</td>
                    <td>
                        <b>
                        <asp:Label ID="ngaydathangLabel0" runat="server" 
                            Text='<%# Bind("ngaydathang") %>' />
                        </b>
                    </td>
                    <td>
                        <b>Số lượng :</b></td>
                    <td>
                        <b>
                        <asp:Label ID="soluongLabel0" runat="server" Text='<%# Bind("soluong") %>' />
                        </b>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        Ngày giao hàng :</td>
                    <td>
                        <b>
                        <asp:Label ID="ngaygiaohangLabel0" runat="server" 
                            Text='<%# Bind("ngaygiaohang") %>' />
                        </b>
                    </td>
                    <td>
                        <b>Đơn giá :</b></td>
                    <td>
                        <b>
                        <asp:Label ID="dongiaLabel0" runat="server" Text='<%# Bind("dongia") %>' />
                        </b>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        Tên người nhận :</td>
                    <td>
                        <b>
                        <asp:Label ID="tennguoinhanLabel" runat="server" 
                            Text='<%# Bind("tennguoinhan") %>' />
                        </b>
                    </td>
                    <td>
                        <b>Thành tiền :</b></td>
                    <td>
                        <b>
                        <asp:Label ID="thanhtienLabel0" runat="server" 
                            Text='<%# Bind("thanhtien") %>' />
                        </b>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        Địa chỉ người nhận :</td>
                    <td>
                        <b>
                        <asp:Label ID="diachinhanLabel" runat="server" 
                            Text='<%# Bind("diachinhan") %>' />
                        </b>
                    </td>
                    <td>
                        <b>Trị giá :</b></td>
                    <td>
                        <b>
                        <asp:Label ID="trigiaLabel0" runat="server" Text='<%# Bind("trigia") %>' />
                        </b>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        Điện thoại người nhận :</td>
                    <td>
                        <b>
                        <asp:Label ID="dienthoainhanLabel0" runat="server" 
                            Text='<%# Bind("dienthoainhan") %>' />
                        </b>
                    </td>
                    <td>
                        <b>Đã giao :</b></td>
                    <td style="font-weight: 700">
                        <asp:CheckBox ID="CheckBox2" runat="server" Text='<%# Bind("dagiao") %>' />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
    </ItemTemplate>
</asp:DataList>



<div style="font-weight: 700; color: #FFFFFF; text-align: center; font-size: x-large; background-color: #FF6600">
    <asp:Label ID="lbthongbao" runat="server"></asp:Label>
</div>




