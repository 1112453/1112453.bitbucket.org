﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Themsanpham.ascx.cs" Inherits="Admin_User_control_Admin_Themsanpham" %>
<%@ Register assembly="CKEditor.NET" namespace="CKEditor.NET" tagprefix="CKEditor" %>
<table cellpadding="10" cellspacing="10" style="width: 100%;">
    <tr>
        <td colspan="2" 
            style="font-weight: 700; color: #FFFFFF; font-size: x-large; background-color: #73B500">
            THÊM SẢN PHẨM</td>
    </tr>
    <tr>
        <td>
            <b>Tên sản phẩm :</b></td>
        <td>
            <asp:TextBox ID="txttensanpham" runat="server" Height="30px" Width="500px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <b>Danh mục sản phẩm :&nbsp; </b> </td>
        <td>
            <asp:DropDownList ID="ddldanhmuc" runat="server" 
                Height="30px" Width="200px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <b>Hãng sản xuất :</b></td>
        <td>
            <asp:DropDownList ID="ddlhangsanxuat" runat="server" Height="30px" 
                Width="200px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <b>Thông số kỹ thuật :</b></td>
        <td>
            <CKEditor:CKEditorControl ID="ckethongsokythuat" runat="server" 
                BasePath="~/Admin/_Samples/ckeditor" 
                ContentsCss="~/Admin/_Samples/ckeditor/contents.css" ToolbarBasic="Bold|Italic|Underline|Strike|-|Subscript|Superscript
NumberedList|BulletedList|-|Outdent|Indent|Blockquote|CreateDiv
JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock
BidiLtr|BidiRtl
Link|Unlink|Anchor
/
Styles|Format|Font|FontSize"></CKEditor:CKEditorControl>
        </td>
    </tr>
    <tr>
        <td>
            <b>Hình minh họa :</b></td>
        <td>
            <asp:FileUpload ID="FileUpload1" runat="server" />
&nbsp;<asp:RequiredFieldValidator ID="rfvupload" runat="server" 
                ControlToValidate="FileUpload1" ErrorMessage="Bạn chưa thêm ảnh sản phẩm.">(*)</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                ControlToValidate="FileUpload1" ErrorMessage="Sai định dạng file" 
                ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.bmp|.BMP|.gif|.GIF|.png|.PNG)$"></asp:RegularExpressionValidator>
            <br />
            <asp:Label ID="lbthongbaoupload" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <b>Đơn giá : </b> </td>
        <td>
            <asp:TextBox ID="txtdongia" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtdongia" ErrorMessage="Chỉ nhập số" ForeColor="Red" 
                ValidationExpression="^\d+$">Chỉ nhập số</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            <b>Bảo hành :</b></td>
        <td>
            <asp:TextBox ID="txtbaohanh" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                ControlToValidate="txtbaohanh" ErrorMessage="Chỉ nhập số" ForeColor="Red" 
                ValidationExpression="^\d+$">Chỉ nhập số</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            <b>Hiển thị sản phẩm :</b></td>
        <td>
            <asp:CheckBox ID="chkhienthi" runat="server" Text="Không hiển thị" />
        </td>
    </tr>
    <tr>
        <td>
            <b></b></td>
        <td style="text-align: center">
            <asp:Button ID="btnthem" runat="server" Height="30px" onclick="btnthem_Click" 
                Text="Thêm sản phẩm" Width="150px" />
&nbsp;<asp:Button ID="btnnhaplai" runat="server" Height="30px" Text="Nhập lại" 
                Width="120px" onclick="btnnhaplai_Click" />
        </td>
    </tr>
    <tr>
        <td>
            <b></b></td>
        <td style="text-align: center">
            <asp:ValidationSummary ID="vsloi" runat="server" ShowMessageBox="True" 
                ShowSummary="False" />
        </td>
    </tr>
    <tr>
        <td>
            <b></b></td>
        <td style="text-align: center">
            <asp:Label ID="lbthongbao" runat="server"></asp:Label>
        </td>
    </tr>
</table>

