﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Khachhang.ascx.cs" Inherits="Admin_User_control_Admin_Khachhang" %>

<table cellpadding="10" cellspacing="10" style="width:100%;">
    <tr>
        <td colspan="2" 
            
            style="font-weight: 700; text-align: center; color: #FFFFFF; font-size: x-large; background-color: #72B400;">
            QUẢN LÝ KHÁCH HÀNG</td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Panel ID="Panel1" runat="server" Visible="False">
                <table style="width:100%;" cellpadding="10" cellspacing="10">
                    <tr>
                        <td style="text-align: left">
                            <b>Tên khách hàng :</b></td>
                        <td>
                            <asp:TextBox ID="txttenkhachhang" runat="server" Height="30px" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Tên đăng nhập :</b></td>
                        <td>
                            <asp:TextBox ID="txttendangnhap" runat="server" Height="30px" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%">
                            <b>Mật khẩu :</b></td>
                        <td>
                            <asp:TextBox ID="txtmatkhau" runat="server" Height="30px" 
                                Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%">
                            <b>Email :</b></td>
                        <td>
                            <asp:TextBox ID="txtemail" runat="server" Height="30px" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Ngày sinh : </b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtngaysinh" runat="server" Height="30px" Width="200px"></asp:TextBox>
                            (MM/dd/yyyy)</td>
                    </tr>
                    <tr>
                        <td>
                            <b>Địa chỉ :</b></td>
                        <td>
                            <asp:TextBox ID="txtdiachi" runat="server" Height="50px" TextMode="MultiLine" 
                                Width="400px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: 700" >
                            Điện thoại :</td>
                        <td >
                            <asp:TextBox ID="txtdienthoai" runat="server" Height="30px" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b></b></td>
                        <td>
                            <asp:Button ID="btncapnhat" runat="server" Height="30px" Text="Cập nhật" 
                                Width="150px" onclick="btncapnhat_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b></b></td>
                        <td>
                            <asp:Label ID="lbthongbaoloi" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                AutoGenerateColumns="False" GridLines="None" style="text-align: center" 
                CellPadding="10" CellSpacing="1" onrowcommand="GridView1_RowCommand" 
                DataKeyNames="matkhau" onpageindexchanging="GridView1_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="ma_kh" HeaderText="Mã khách hàng" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="hoten" HeaderText="Họ Tên" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ten_dn" HeaderText="Tên đăng nhập" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="matkhau" HeaderText="Mật khẩu" Visible="False" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="email" HeaderText="Email" >
                    <HeaderStyle ForeColor="White" Height="30px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ngaysinh" HeaderText="Ngày sinh" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="gioitinh" HeaderText="Giới Tính" >
                    <HeaderStyle ForeColor="White" />
                    </asp:CheckBoxField>
                    <asp:BoundField DataField="diachi" HeaderText="Địa chỉ" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="dienthoai" HeaderText="Điện thoại" >
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:ButtonField CommandName="nutsua" HeaderText="Sửa" Text="Sửa">
                    <HeaderStyle ForeColor="White" />
                    </asp:ButtonField>
                    <asp:ButtonField CommandName="nutxoa" HeaderText="Xóa" Text="Xóa">
                    <HeaderStyle ForeColor="White" />
                    </asp:ButtonField>
                </Columns>
                <EmptyDataRowStyle BackColor="White" />
                <HeaderStyle BackColor="#65AA00" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
</table>


