﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_User_control_Admin_doimatkhau : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["user_admin"] == null)
                Response.Redirect("~/Dangnhap.aspx");
        }
    }
    protected void btndoimatkhau_Click(object sender, EventArgs e)
    {
        try
        {
            txtmatkhauhientai.Text = mahoa_md5.Encrypt(txtmatkhauhientai.Text);
            txtmatkhaumoi.Text = mahoa_md5.Encrypt(txtmatkhaumoi.Text);
            string matkhau = txtmatkhauhientai.Text;
            string tendn = Session["user_admin"].ToString();
            string matkhaumoi = txtmatkhaumoi.Text;
            string lenhsql = "select * from admin where ten_dn='" + tendn + "'and matkhau='" + matkhau + "'";
            DataTable dt = xulidulieu.Docbang(lenhsql);
            if (dt.Rows.Count == 0)
            {
                lbthongbao.Text = "Sai tên đăng nhập hoặc mật khẩu";
            }
            else
            {
                //Thực thi lệnh điều chỉnh dữ liệu
                string str = "update admin set matkhau='" + matkhaumoi + "'where ten_dn ='" + tendn + "'";
                xulidulieu.thuchienlenh(str);
                Response.Redirect("~/Admin/Default.aspx");
            }
        }
        catch (Exception)
        {
            lbthongbao.Text = "Thay đổi mật khẩu thất bại!";
        }

    }
}