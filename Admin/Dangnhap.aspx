﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Dangnhap.aspx.cs" Inherits="Admin_Dangnhap" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Đăng nhập | Khu vực quản trị</title>
    <style type="text/css">
    #form1
    {
        background-image:url(styles/images/login-box-backg.png);
        width:485px;
        height:410px;
        margin:20px 20px 20px 20px;
        margin: 0 auto;
        margin-top:100px;
    }
    
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table cellpadding="10" cellspacing="10" style="width:100%;">
            <tr>
                <td colspan="4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" 
                    style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: large; font-weight: bold; color: #FFFFFF;">
                    ĐĂNG NHẬP | KHU VỰC QUẢN TRỊ</td>
            </tr>
            <tr>
                <td rowspan="4">
                    &nbsp;</td>
                <td style="color: #FFFFFF; font-weight: 700">
                    Tên đăng nhập :</td>
                <td>
                    <asp:TextBox ID="txttendangnhap" runat="server" Height="30px" Width="150px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="rgedn" runat="server" 
                        ControlToValidate="txttendangnhap" ErrorMessage="Tên đăng nhập không hợp lệ!" 
                        ValidationExpression="^[a-zA-Z'.\s]{1,40}$">(*)</asp:RegularExpressionValidator>
                </td>
                <td rowspan="4">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="color: #FFFFFF; font-weight: 700">
                    Mật khẩu :</td>
                <td>
                    <asp:TextBox ID="txtmatkhau" runat="server" Height="30px" TextMode="Password" 
                        Width="150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:ImageButton ID="imbtlogin" runat="server" 
                        ImageUrl="~/Admin/styles/images/login-btn.png" onclick="imbtlogin_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <asp:Label ID="lbthongbao" runat="server" style="color: #800000"></asp:Label>
                    <br />
                    <asp:ValidationSummary ID="vsthongbao" runat="server" ShowMessageBox="True" 
                        ShowSummary="False" />
                </td>
            </tr>
        </table>

    </div>
    </form>
</body>
</html>
