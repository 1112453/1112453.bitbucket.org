﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Admin_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1"  Runat="Server">
<style type="text/css">
.spacing
{
    padding-bottom:200px;
    }
</style>
	<h3>
        <asp:Label ID="lbtendn" runat="server"></asp:Label>
    </h3>
			<p id="page-intro">Danh mục trang quản trị</p>
			
			<ul class="shortcut-buttons-set">
				
				<li><a class="shortcut-button" href="Default.aspx"><span>

					<img src="styles/images/icons/icon-home.png" alt="icon" height="80px" 
                        width="90px"><br>
					Trang chủ
				</span></a></li>
				
				<li><a class="shortcut-button" href="Khachhang.aspx"><span>
					<img src="styles/images/icons/khachhang.png" alt="icon" height="80px" 
                        width="90px"><br>
					Khách hàng
				</span></a></li>
				
				<li><a class="shortcut-button" href="Sanpham.aspx"><span>
					<img src="styles/images/icons/icon_product.png" alt="icon" height="80px" 
                        width="90px"><br>
					Sản phẩm
				</span></a></li>

				
				<li><a class="shortcut-button" href="Hoadon.aspx"><span>
					<img src="styles/images/icons/gio_hang_icon.jpg" alt="icon" height="80px" 
                        width="90px"><br>
					Đơn đặt hàng
				</span></a></li>
				
			</ul>
            <div class="spacing"></div>
</asp:Content>

