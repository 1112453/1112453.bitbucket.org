﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Sanpham.ascx.cs" Inherits="Admin_User_control_Admin_Sanpham" %>

<%@ Register assembly="CKEditor.NET" namespace="CKEditor.NET" tagprefix="CKEditor" %>

<%@ Register assembly="CollectionPager" namespace="SiteUtils" tagprefix="cc1" %>

<style type="text/css">
.visible
{
    display:none;
    }
</style>
<table cellpadding="10" cellspacing="10" style="width:100%;">
    <tr>
        <td 
            style="color: #000000; text-align: center; ">
            <table style="width:100%;">
                <tr>
                    <td style="color: #FFFFFF; background-color: #74B500;">
            <strong style="font-weight: bold; color: #FFFFFF; font-size: x-large; font-family: Arial, Helvetica, sans-serif;">DACH SÁCH SẢN PHẨM</strong></td>
                </tr>
            </table>
            <strong>
            <br />
            </strong><div>
                <asp:Panel ID="Panel1" runat="server" Visible="False">
                    <table cellpadding="10" cellspacing="10" style="width: 100%;">
                        <tr>
                            <td>
                                Tên sản phẩm :</td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txttensanpham" runat="server" Height="30px" Width="500px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Danh mục sản phẩm :&nbsp;
                            </td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddldanhmuc" runat="server" 
                Height="30px" Width="200px" style="text-align: left">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Hãng sản xuất :</td>
                            <td style="text-align: left">
                                <asp:DropDownList ID="ddlhangsanxuat" runat="server" Height="30px" 
                Width="200px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Thông số kỹ thuật :</td>
                            <td>
                                <CKEditor:CKEditorControl ID="ckethongsokythuat" runat="server" 
                                    BasePath="~/Admin/_Samples/ckeditor" 
                                    ContentsCss="~/Admin/_Samples/ckeditor/contents.css" 
                                    TemplatesFiles="~/Admin/_Samples/ckeditor/plugins/templates/templates/default.js"></CKEditor:CKEditorControl>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Hình minh họa :</td>
                            <td style="text-align: left">
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfvupload" runat="server" 
                ControlToValidate="FileUpload1" ErrorMessage="Bạn chưa thêm ảnh sản phẩm.">(*)</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                    ControlToValidate="FileUpload1" ErrorMessage="File upload không hợp lệ!" 
                                    ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.jpg|.JPG|.jpeg|.JPEG|.bmp|.BMP|.gif|.GIF|.png|.PNG)$">(*)</asp:RegularExpressionValidator>
                                <br />
                                <asp:Label ID="lbthongbaoupload" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Đơn giá :
                            </td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtdongia" runat="server" Height="30px" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtdongia" ErrorMessage="Chỉ nhập số" ForeColor="Red" 
                ValidationExpression="^\d+$">Chỉ nhập số</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Bảo hành :</td>
                            <td style="text-align: left">
                                <asp:TextBox ID="txtbaohanh" runat="server" Height="30px" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                ControlToValidate="txtbaohanh" ErrorMessage="Chỉ nhập số" ForeColor="Red" 
                ValidationExpression="^\d+$">Chỉ nhập số</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Hiển thị sản phẩm :</td>
                            <td style="text-align: left">
                                <asp:CheckBox ID="chkhienthi" runat="server" Text="Không hiển thị" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td style="text-align: left">
                                <asp:Button ID="btnthem" runat="server" Height="30px" onclick="btnthem_Click" 
                Text="Cập nhật" Width="150px" />
                                &nbsp;<asp:Button ID="btnnhaplai" runat="server" Height="30px" Text="Nhập lại" 
                Width="120px" onclick="btnnhaplai_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td style="text-align: center">
                                <asp:ValidationSummary ID="vsloi" runat="server" ShowMessageBox="True" 
                                    ShowSummary="False" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td style="text-align: center">
                                <asp:Label ID="lbthongbao" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                CellPadding="10" CellSpacing="1" 
                onrowcommand="GridView1_RowCommand" DataKeyNames="chitietsp" 
                AllowPaging="True" PageSize="10" 
                onpageindexchanging="GridView1_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="ma_sp" HeaderText="Mã sản phẩm">
                    <HeaderStyle Font-Bold="True" ForeColor="White" Height="30px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ten_sp" HeaderText="Tên sản phẩm">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ten_dm" HeaderText="Tên danh mục">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ten_hang" HeaderText="Tên hãng sx">
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="chitietsp" Visible="False" >
                    <HeaderStyle ForeColor="White" Font-Bold="True" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Hình minh họa">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("hinhminhhoa") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" Height="70px" 
                                ImageUrl='<%# Eval("hinhminhhoa","~/images/sanpham/{0}") %>' Width="70px" />
                        </ItemTemplate>
                        <HeaderStyle Font-Bold="True" ForeColor="White" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="dongia" HeaderText="Đơn giá" >
                    <HeaderStyle ForeColor="White" Font-Bold="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="baohanh" HeaderText="Bảo hành" >
                    <HeaderStyle ForeColor="White" Font-Bold="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="soluongban" HeaderText="Số lượng bán" >
                    <HeaderStyle ForeColor="White" Font-Bold="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ngaycapnhat" HeaderText="Ngày cập nhật" >
                    <HeaderStyle ForeColor="White" Font-Bold="True" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="dang" HeaderText="Đăng" >
                    <HeaderStyle ForeColor="White" Font-Bold="True" />
                    </asp:CheckBoxField>
                    <asp:ButtonField CommandName="nutsua" Text="Sửa">
                    <HeaderStyle ForeColor="White" Font-Bold="True" />
                    </asp:ButtonField>
                    <asp:ButtonField CommandName="nutxoa" Text="Xóa">
                    <HeaderStyle ForeColor="White" Font-Bold="True" />
                    </asp:ButtonField>
                </Columns>
                <HeaderStyle BackColor="#6CAF00" Font-Bold="True" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
</table>

