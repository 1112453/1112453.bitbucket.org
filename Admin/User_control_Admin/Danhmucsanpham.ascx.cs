﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_User_control_Admin_Danhmucsanpham : System.Web.UI.UserControl
{
    public static int madm;
    public static bool _c=false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user_admin"] != null)
        {
            string str = "select * from danhmucsanpham";
            DataTable dt = xulidulieu.Docbang(str);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        else
            Response.Redirect("~/Admin/Dangnhap.aspx");
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "nutxoa")
        {
            int chiso = int.Parse(e.CommandArgument.ToString());
            madm = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            try
            {
                string str = "delete from danhmucsanpham where ma_dm='"+madm+"'";
                xulidulieu.thuchienlenh(str);
                Response.Redirect("~/Admin/Quanlydanhmuc.aspx");
            }
            catch (Exception)
            {
                lbthongbaoloi.Visible = true;
                lbthongbaoloi.Text = "<script>alert('Xóa không thành công');</script>";
            }
        }
        if (e.CommandName == "nutsua")
        {

            _c = true;
            int chiso = int.Parse(e.CommandArgument.ToString());
            madm = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            string tendm=HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[1].Text));
            txttendanhmuc.Text = tendm;
        }
    }
    protected void btnluu_Click(object sender, EventArgs e)
    {
        string tendm=txttendanhmuc.Text;
        if (_c == false)
        {
            string str = "INSERT INTO danhmucsanpham (ten_dm) VALUES (N'" + tendm + "')";
            xulidulieu.thuchienlenh(str);
            Response.Redirect("~/Admin/Quanlydanhmuc.aspx");
            txttendanhmuc.Text = "";
            txttendanhmuc.Focus();
            lbthongbaoloi.Visible = true;
            lbthongbaoloi.Text = "<script>alert('Thêm danh mục sản phẩm thành công');</script>";
        }
        else
        {
            string str = "update danhmucsanpham set ten_dm=N'" + tendm + "' where ma_dm='"+madm+"'";
            xulidulieu.thuchienlenh(str);
            Response.Redirect("~/Admin/Quanlydanhmuc.aspx");
            lbthongbaoloi.Visible = true;
            lbthongbaoloi.Text = "<script>alert('Cập nhật thành công');</script>";
        }
    }
}