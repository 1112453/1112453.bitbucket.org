﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Tintuc.ascx.cs" Inherits="Admin_User_control_Admin_Tintuc" %>
<%@ Register assembly="CKEditor.NET" namespace="CKEditor.NET" tagprefix="CKEditor" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<style type="text/css">
.visibecol
{
    display:none;
    }
</style>
<table cellpadding="10" cellspacing="10" style="width:100%;">
    <tr>
        <td style="text-align: center; font-weight: 700; color: #FFFFFF; font-size: x-large; background-color: #72B400">
            QUẢN LÝ TIN TỨC</td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="Panel1" runat="server" Visible="False">
                <table cellpadding="10" cellspacing="10" 
    style="width:100%;">
                    <tr>
                        <td>
                            <b>Tiêu đề :</b></td>
                        <td>
                            <asp:TextBox ID="txttieude" runat="server" Height="30px" 
                Width="600px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Mô tả :</b></td>
                        <td>
                            <CKEditor:CKEditorControl ID="ckemota" runat="server" 
                ContentsCss="~/Admin/ckeditor/contents.css" 
                TemplatesFiles="~/Admin/_Samples/ckeditor/plugins/templates/templates/default.js" 
                Toolbar="Basic" BasePath="~/Admin/_Samples/ckeditor">
            </CKEditor:CKEditorControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Nội dung :</b></td>
                        <td>
                            <CKEditor:CKEditorControl ID="ckenoidung" runat="server" 
                                BasePath="~/Admin/_Samples/ckeditor" 
                                ContentsCss="~/Admin/_Samples/ckeditor/contents.css" 
                                TemplatesFiles="~/Admin/_Samples/ckeditor/plugins/templates/templates/default.js">
            </CKEditor:CKEditorControl>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Hình minh họa :</b></td>
                        <td>
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                            <br />
                            <asp:Label ID="lbthongbaoupload" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: 700">
                            Hiển thị</td>
                        <td>
                            <asp:CheckBox ID="chkhienthi" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b></b></td>
                        <td style="text-align: center">
                            <asp:Button ID="btnluu" runat="server" Text="Lưu" Width="120px" 
                                onclick="btnluu_Click" />
                            &nbsp;<asp:Button ID="btnhuy" runat="server" Text="Hủy" Width="120px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <asp:Label ID="lbthongbao" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                CellPadding="10" CellSpacing="1" GridLines="None" 
                onrowcommand="GridView1_RowCommand" >
                <Columns>
                    <asp:BoundField DataField="ma_tt" HeaderText="Mã tin" />
                    <asp:BoundField DataField="tieude" HeaderText="Tiêu đề" />
                    <asp:BoundField DataField="mota" HeaderText="Mô tả" />
                    <asp:BoundField DataField="noidung" ItemStyle-CssClass="visibecol">
<ItemStyle CssClass="visibecol"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="hinhminhhoa" HeaderText="Hình minh họa" />
                    <asp:BoundField DataField="ngaycapnhat" HeaderText="Ngày cập nhật" />
                    <asp:BoundField DataField="nguoitao" HeaderText="Người tạo" />
                    <asp:BoundField DataField="dang" HeaderText="Đăng" />
                    <asp:ButtonField CommandName="nutxoa" HeaderText="Xóa" Text="Xóa" />
                    <asp:ButtonField CommandName="nutsua" HeaderText="Sửa" Text="Sửa" />
                </Columns>
                <HeaderStyle BackColor="#72B400" Font-Bold="True" ForeColor="White" />
            </asp:GridView>
        </td>
    </tr>
</table>

