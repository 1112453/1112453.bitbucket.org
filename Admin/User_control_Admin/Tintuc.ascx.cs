﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_User_control_Admin_Tintuc : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["user_admin"] != null)
            {
                string str = "select * from tintuc";
                DataTable dt = xulidulieu.Docbang(str);
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
                Response.Redirect("~/Admin/Dangnhap.aspx");
        }
    }
    protected void btnluu_Click(object sender, EventArgs e)
    {
        //upload ảnh
        string hinhanh;
        hinhanh = FileUpload1.FileName;
        FileUpload1.SaveAs(MapPath("~/images/sanpham/news" + hinhanh));
        lbthongbaoupload.Text = "Up ảnh thành công";
        //
        try
        {
            string tieude = txttieude.Text;
            string mota = ckemota.Text;
            string noidung = ckenoidung.Text;
            string ngaycapnhat = DateTime.Today.ToShortDateString();
            bool hienthi;
            if (chkhienthi.Checked == true)
                hienthi = false;
            else
                hienthi = true;
            string nguoitao = Session["user_admin"].ToString();
            string str = string.Format("insert into tintuc(tieude,mota,noidung,hinhminhhoa,ngaycapnhat,nguoitao,dang) values (N'{0}',N'{1}',N'{2}','{3}','{4}','{5}','{6}')",tieude,mota,noidung,hinhanh,ngaycapnhat,nguoitao, hienthi);

            xulidulieu.thuchienlenh(str);
            Response.Redirect("~/Admin/Tintuc.aspx");
        }
        catch (Exception)
        {
            lbthongbao.Text = "Không thêm được tin mới";
        }
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "nutsua")
        {
            Panel1.Visible = true;
        }
    }
}