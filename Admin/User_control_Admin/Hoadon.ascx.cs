﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_User_control_Admin_Hoadon : System.Web.UI.UserControl
{
    public static int sodh;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["user_admin"] != null)
            {
                string str = "select * from dathang dh,chitietdathang ctdh,sanpham sp,khachhang kh where ctdh.sodh=dh.sodh and ctdh.ma_sp=sp.ma_sp and kh.ma_kh=dh.ma_kh";
                DataTable dt = xulidulieu.Docbang(str);
                if (dt.Rows.Count == 0)
                    lbthongbaoloi.Text = "Chưa có đơn đặt hàng nào!";
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {
                Response.Redirect("~/Admin/Dangnhap.aspx");
            }
        }

    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "nutxoa")
        {
            int chiso = int.Parse(e.CommandArgument.ToString());
            sodh = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            try
            {
                string str1 = "delete from dathang where sodh='" + sodh + "'";
                string str2 = "delete from chitietdathang where sodh='" + sodh + "'";
                xulidulieu.thuchienlenh(str2);
                xulidulieu.thuchienlenh(str1);
                Response.Redirect("~/Admin/Hoadon.aspx");
            }
            catch (Exception)
            {
                lbthongbaoloi.Visible = true;
                lbthongbaoloi.Text = "Xóa không thành công";
            }
        }
        if (e.CommandName == "nutsua")
        {
            string str = "select dagiao from dathang";
            DataTable dt = xulidulieu.Docbang(str);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["dagiao"].ToString() == "True")
                        chkdagiao.Checked = true;
                    else
                        chkdagiao.Checked = false;
                }
            }
            Panel1.Visible = true;
            int chiso = int.Parse(e.CommandArgument.ToString());
            sodh = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            string tenkhachhang = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[4].Text));
            string tennguoinhan = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[12].Text));
            string diachinhan = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[13].Text));
            string dienthoainhan = GridView1.Rows[chiso].Cells[14].Text;
            if (GridView1.Rows[chiso].Cells[10].Text =="True")
                chkdagiao.Checked = true;
            else
                chkdagiao.Checked = false;
            txtennguoinhan.Text = tennguoinhan;
            txtdiachinhan.Text = diachinhan;
            txtdienthoainhan.Text = dienthoainhan;
            lbtenkhachhang.Text = tenkhachhang;
        }
    }
    protected void btnhuy_Click(object sender, EventArgs e)
    {
        txtennguoinhan.Text = "";
        txtdiachinhan.Text = "";
        txtdienthoainhan.Text = "";
        txtennguoinhan.Focus();
    }
    protected void btncapnhat_Click(object sender, EventArgs e)
    {
        string tennguoinhan=txtennguoinhan.Text;
        string diachinhan=txtdiachinhan.Text;
        string dienthoainhan=txtdienthoainhan.Text;
        string dagiao;
        if(chkdagiao.Checked==true)
            dagiao="true";
        else
            dagiao="false";
        string str="update dathang set tennguoinhan=N'"+tennguoinhan+"',diachinhan=N'"+diachinhan+"',dienthoainhan='"+dienthoainhan+"',dagiao='"+dagiao+"' where sodh="+sodh;
        xulidulieu.thuchienlenh(str);
        Response.Redirect("~/Admin/Hoadon.aspx");
    }
}