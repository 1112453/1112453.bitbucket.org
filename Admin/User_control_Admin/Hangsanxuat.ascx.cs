﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Admin_User_control_Admin_Hangsanxuat : System.Web.UI.UserControl
{
    public static int ma_hangsx;
    public static bool _c = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["user_admin"] != null)
            {
                string str = "select * from hangsanxuat";
                DataTable dt = xulidulieu.Docbang(str);
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
                Response.Redirect("~/Admin/Dangnhap.aspx");
        }
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "nutxoa")
        {
            int chiso = int.Parse(e.CommandArgument.ToString());
            ma_hangsx = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            try
            {
                string str = "delete from hangsanxuat where ma_hangsx='"+ma_hangsx+"'";
                xulidulieu.thuchienlenh(str);
                Response.Redirect("~/Admin/Quanlydanhmuc.aspx");
            }
            catch (Exception)
            {
                lbthongbaoloi.Visible = true;
                lbthongbaoloi.Text = "<script>alert('Xóa không thành công');</script>";
            }
        }
        if (e.CommandName == "nutsua")
        {

            _c = true;
            int chiso = int.Parse(e.CommandArgument.ToString());
            ma_hangsx = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            string ten_hang=HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[1].Text));
            txthangsanxuat.Text = ten_hang;
        }
    }
    protected void btnluu_Click(object sender, EventArgs e)
    {
        string ten_hang=txthangsanxuat.Text;
        if (_c == false)
        {
            string str = "INSERT INTO hangsanxuat (ten_hang) VALUES (N'" + ten_hang + "')";
            xulidulieu.thuchienlenh(str);
            Response.Redirect("~/Admin/Hangsanxuat.aspx");
            txthangsanxuat.Text = "";
            txthangsanxuat.Focus();
            lbthongbaoloi.Visible = true;
            lbthongbaoloi.Text = "<script>alert('Thêm hãng sản xuất thành công');</script>";
        }
        else
        {
            string str = "update hangsanxuat set ten_hang=N'" + ten_hang + "' where ma_hangsx='"+ma_hangsx+"'";
            xulidulieu.thuchienlenh(str);
            Response.Redirect("~/Admin/Hangsanxuat.aspx");
            lbthongbaoloi.Visible = true;
            lbthongbaoloi.Text = "<script>alert('Cập nhật thành công');</script>";
        }
    }
}