﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_User_control_Admin_Sanpham : System.Web.UI.UserControl
{
    public static string hinhanh;
    public static bool _c;
    public static int ma_sp;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            getdata();
            Loaddmsanpham();
            Loadhangsanxuat();
            
        }
        if (Session["user_admin"] != null)
        {
            getdata();
        }
        else
            Response.Redirect("~/Admin/Dangnhap.aspx");
    }
   public DataTable getdata()
    {
        string str = "select * from sanpham s,danhmucsanpham d,hangsanxuat h where s.ma_dm=d.ma_dm and s.ma_hangsx=h.ma_hangsx order by ngaycapnhat";
        DataTable dt = xulidulieu.Docbang(str);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        return dt;
    }
    public void Loaddmsanpham()
    {
        string sql = "select ma_dm, ten_dm from danhmucsanpham";
        DataTable dt = xulidulieu.Docbang(sql);
        ddldanhmuc.DataSource = dt;
        ddldanhmuc.DataTextField = dt.Columns["ten_dm"].ToString();   // chữ
        ddldanhmuc.DataValueField = dt.Columns["ma_dm"].ToString(); // giá trị
        ddldanhmuc.DataBind();

    }
    public void Loadhangsanxuat()
    {
        string sql = "select ma_hangsx, ten_hang from hangsanxuat";
        DataTable dt = xulidulieu.Docbang(sql);
        ddlhangsanxuat.DataSource = dt;
        ddlhangsanxuat.DataTextField = dt.Columns["ten_hang"].ToString();   // chữ
        ddlhangsanxuat.DataValueField = dt.Columns["ma_hangsx"].ToString(); // giá trị
        ddlhangsanxuat.DataBind();
    }

    protected void btnthem_Click(object sender, EventArgs e)
    {
        if (_c == false)
        {
            //upload ảnh
            if (FileUpload1.HasFile)
            {
                hinhanh = FileUpload1.FileName;
                FileUpload1.SaveAs(MapPath("~/images/sanpham/" + hinhanh));
                lbthongbaoupload.Text = "Up ảnh thành công";
            }
            //
            try
            {
                string tensp = txttensanpham.Text;
                string chitiet = ckethongsokythuat.Text;
                string dongia = txtdongia.Text;
                string baohanh = txtbaohanh.Text;
                string ngaycapnhat = DateTime.Today.ToShortDateString();
                string hienthi;
                string danhmuc = ddldanhmuc.SelectedItem.Text;
                string hangsanxuat = ddlhangsanxuat.SelectedItem.Text;
                string ma_dm = ddldanhmuc.SelectedItem.Value;
                string ma_hangsx = ddlhangsanxuat.SelectedItem.Value;
                if (chkhienthi.Checked == true)
                    hienthi = "0";
                else
                    hienthi = "1";
                string str = string.Format("insert into sanpham(ma_hangsx,ma_dm,ten_sp,chitietsp,hinhminhhoa,dongia,baohanh,ngaycapnhat,dang) values ('{0}','{1}',N'{2}',N'{3}',N'{4}','{5}','{6}','{7}','{8}')", ma_hangsx, ma_dm, tensp, chitiet, hinhanh, dongia, baohanh, ngaycapnhat, hienthi);
                xulidulieu.thuchienlenh(str);
                Response.Redirect("~/Admin/Sanpham.aspx");
            }
            catch (Exception)
            {
                lbthongbao.Text = "Không thêm được sản phẩm";
            }
        }
        else
        {
            string tensp = txttensanpham.Text;
            string chitiet = ckethongsokythuat.Text;
            string dongia = txtdongia.Text;
            string baohanh = txtbaohanh.Text;
            string ngaycapnhat = DateTime.Today.ToShortDateString();
            string hienthi;
            string danhmuc = ddldanhmuc.SelectedItem.Text;
            string hangsanxuat = ddlhangsanxuat.SelectedItem.Text;
            string ma_dm = ddldanhmuc.SelectedItem.Value;
            string ma_hangsx = ddlhangsanxuat.SelectedItem.Value;
            if (chkhienthi.Checked == true)
                hienthi = "false";
            else
                hienthi = "true";
            string str = "update sanpham set ma_dm='" + ma_dm + "',ma_hangsx='" + ma_hangsx + "',ten_sp=N'" + tensp + "',chitietsp=N'" + chitiet + "',dongia='" + dongia + "',baohanh='" + baohanh + "',dang='" + hienthi + "' where ma_sp=" + ma_sp;
            xulidulieu.thuchienlenh(str);
            _c = false;
            Response.Redirect("~/Admin/Sanpham.aspx");
            lbthongbao.Visible = true;
            lbthongbao.Text = "<script>alert('Cập nhật thành công');</script>";
        }
    }

    protected void btnnhaplai_Click(object sender, EventArgs e)
    {
        txtbaohanh.Text = "";
        txtdongia.Text = "";
        txttensanpham.Text = "";
        ckethongsokythuat.Text = "";
        txttensanpham.Focus();
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "nutxoa")
        {
            int chiso = int.Parse(e.CommandArgument.ToString());
            ma_sp = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            try
            {
                string str = "delete from sanpham where ma_sp='" + ma_sp + "'";
                xulidulieu.thuchienlenh(str);
                Response.Redirect("~/Admin/Sanpham.aspx");
            }
            catch (Exception)
            {
                lbthongbao.Visible = true;
                lbthongbao.Text = "<script>alert('Xóa không thành công');</script>";
            }
        }
        if (e.CommandName == "nutsua")
        {
            string str = "select dang,chitietsp from sanpham";
            DataTable dt = xulidulieu.Docbang(str);
            Loaddmsanpham();
            Loadhangsanxuat();
            Panel1.Visible = true;
            _c = true;
            int chiso = int.Parse(e.CommandArgument.ToString());
            ma_sp = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            string tensp = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[1].Text));
            string danhmucsp = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[2].Text));
            string hangsx = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[3].Text));
            string hinhminhhoa = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[5].Text));
            string dongia = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[6].Text));
            string baohanh = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[7].Text));
            string chitiet = HttpUtility.HtmlEncode((string)GridView1.DataKeys[0].Value.ToString());

            if (GridView1.Rows[chiso].Cells[10].Text == "True")
                chkhienthi.Checked = true;
            txttensanpham.Text = tensp;
            txtdongia.Text = dongia;
            txtbaohanh.Text = baohanh;
            ddldanhmuc.SelectedItem.Text = danhmucsp;
            ddlhangsanxuat.SelectedItem.Text = hangsx;
            lbthongbaoupload.Text = hinhminhhoa;
            rfvupload.Enabled = false;
            ckethongsokythuat.Text = chitiet;
            }
        }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        this.getdata();
    }
}