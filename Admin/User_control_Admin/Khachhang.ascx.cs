﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Admin_User_control_Admin_Khachhang : System.Web.UI.UserControl
{
    public static int makh;
    public static bool _c = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["user_admin"] != null)
            {
                getdata();
            }
            else
                Response.Redirect("~/Admin/Dangnhap.aspx");
        }
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "nutxoa")
        {
            int chiso = int.Parse(e.CommandArgument.ToString());
            makh = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            try
            {
                string str = "delete from khachhang where ma_kh='" + makh + "'";
                xulidulieu.thuchienlenh(str);
                Response.Redirect("~/Admin/Khachhang.aspx");
                lbthongbaoloi.Text = "<script>alert('Đã xóa thành công');</script>";
            }
            catch (Exception)
            {
                lbthongbaoloi.Visible = true;
                lbthongbaoloi.Text = "<script>alert('Xóa không thành công');</script>";
            }
        }
        if (e.CommandName == "nutsua")
        {
            Panel1.Visible = true;
            _c = true;
            int chiso = int.Parse(e.CommandArgument.ToString());
            makh = int.Parse(GridView1.Rows[chiso].Cells[0].Text.ToString());
            string hoten = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[1].Text));
            string tendn=HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[2].Text));
            string matkhau = GridView1.DataKeys[0].Value.ToString();
            string email=HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[4].Text));
            string ngaysinh = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[5].Text));
            string diachi = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[7].Text));
            string dienthoai = HttpUtility.HtmlDecode((string)(GridView1.Rows[chiso].Cells[8].Text));
            txttendangnhap.Text = tendn;
            txtmatkhau.Text=matkhau;
            txtngaysinh.Text = ngaysinh;
            txttenkhachhang.Text = hoten;
            txtemail.Text =email;
            txtdiachi.Text = diachi;
            txtdienthoai.Text = dienthoai;
            txttendangnhap.Focus();
        }
    }
    protected void btncapnhat_Click(object sender, EventArgs e)
    {
        string str = "update khachhang set ten_dn=N'" + txttendangnhap.Text + "',matkhau='"+txtmatkhau.Text+"',email='"+txtemail.Text+"',hoten=N'"+txttenkhachhang.Text+"',ngaysinh='"+txtngaysinh.Text+"',dienthoai='"+txtdienthoai.Text+"',diachi=N'"+txtdiachi.Text+"' where ma_kh=" + makh;
        xulidulieu.thuchienlenh(str);
        Response.Redirect("~/Admin/Khachhang.aspx");
        lbthongbaoloi.Visible = true;
        lbthongbaoloi.Text = "<script>alert('Cập nhật thành công');</script>";
    }
    public DataTable getdata()
    {
        string str = "select * from khachhang";
        DataTable dt = xulidulieu.Docbang(str);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        return dt;
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        this.getdata();
    }
}