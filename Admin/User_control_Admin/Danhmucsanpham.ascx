﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Danhmucsanpham.ascx.cs" Inherits="Admin_User_control_Admin_Danhmucsanpham" %>
<table cellpadding="10" cellspacing="10" style="width:100%;" width="900">
    <tr>
        <td colspan="2" 
            
            style="text-align: center; font-weight: 700; color: #FFFFFF; font-size: x-large; background-color: #6CAF00;">
            QUẢN LÝ DANH MỤC SẢN PHẨM</td>
    </tr>
    <tr>
        <td colspan="2" style="font-weight: 700; color: #6666FF">
            THÊM DANH MỤC SẢN PHẨM</td>
    </tr>
    <tr>
        <td style="text-align: right; font-weight: 700;" width="300">
            Tên danh mục sản phẩm:</td>
        <td>
            <asp:TextBox ID="txttendanhmuc" runat="server" Height="30px" 
                style="text-align: left" Width="300px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="text-align: right" width="300">
            &nbsp;</td>
        <td>
            <asp:Button ID="btnluu" runat="server" Height="30px" 
                Text="Lưu" Width="120px" onclick="btnluu_Click" />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: left; font-weight: 700; color: #6666FF;" 
            width="300">
            QUẢN LÝ DANH MỤC</td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: left" width="300">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                CellPadding="10" CellSpacing="1" GridLines="None" 
                onrowcommand="GridView1_RowCommand" Width="100%">
                <Columns>
                    <asp:BoundField DataField="ma_dm" HeaderText="Mã danh mục">
                    <HeaderStyle Height="30px" Font-Bold="True" ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ten_dm" HeaderText="Tên danh mục sản phẩm" >
                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                    </asp:BoundField>
                    <asp:ButtonField CommandName="nutsua" Text="Sửa" />
                    <asp:ButtonField CommandName="nutxoa" Text="Xóa" />
                </Columns>
                <HeaderStyle BackColor="#6CAF00" />
            </asp:GridView>
            <asp:Label ID="lbthongbaoloi" runat="server" 
                style="font-weight: 700; color: #FF0000" Visible="False"></asp:Label>
        </td>
    </tr>
</table>

