﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Admin_User_control_Admin_Themsanpham : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["user_admin"] == null)
                Response.Redirect("~/Admin/Dangnhap.aspx");
            Loaddmsanpham();
            Loadhangsanxuat();
        }
    }

    public void Loaddmsanpham()
    {
        string strconn = ConfigurationManager.ConnectionStrings["vnaovn_shopConnectionString"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(strconn);
        string sql = "select ma_dm, ten_dm from danhmucsanpham";
        DataTable dt = xulidulieu.Docbang(sql);
        ddldanhmuc.DataSource = dt;
        ddldanhmuc.DataTextField = dt.Columns["ten_dm"].ToString();   // chữ
        ddldanhmuc.DataValueField = dt.Columns["ma_dm"].ToString(); // giá trị
        ddldanhmuc.DataBind();

    }
    public void Loadhangsanxuat()
    {
        string strconn = ConfigurationManager.ConnectionStrings["vnaovn_shopConnectionString"].ConnectionString.ToString();
        SqlConnection conn = new SqlConnection(strconn);
        string sql = "select ma_hangsx, ten_hang from hangsanxuat";
        DataTable dt = xulidulieu.Docbang(sql);
        ddlhangsanxuat.DataSource = dt;
        ddlhangsanxuat.DataTextField = dt.Columns["ten_hang"].ToString();   // chữ
        ddlhangsanxuat.DataValueField = dt.Columns["ma_hangsx"].ToString(); // giá trị
        ddlhangsanxuat.DataBind();

    }
    protected void btnthem_Click(object sender, EventArgs e)
    {
        //upload ảnh
        string hinhanh;
        hinhanh = FileUpload1.FileName;
        FileUpload1.SaveAs(MapPath("~/images/sanpham/" + hinhanh));
        lbthongbaoupload.Text = "Up ảnh thành công";
        //
        try
        {
            string tensp = txttensanpham.Text;
            string chitiet = ckethongsokythuat.Text;
            string dongia = txtdongia.Text;
            string baohanh = txtbaohanh.Text;
            string ngaycapnhat = DateTime.Today.ToShortDateString();
            string hienthi;
            string danhmuc = ddldanhmuc.SelectedItem.Text;
            string hangsanxuat = ddlhangsanxuat.SelectedItem.Text;
            string ma_dm = ddldanhmuc.SelectedItem.Value;
            string ma_hangsx = ddlhangsanxuat.SelectedItem.Value;
            if (chkhienthi.Checked == true)
                hienthi = "0";
            else
                hienthi = "1";
            string str = string.Format("insert into sanpham(ma_hangsx,ma_dm,ten_sp,chitietsp,hinhminhhoa,dongia,baohanh,ngaycapnhat,dang) values ('{0}','{1}',N'{2}',N'{3}',N'{4}','{5}','{6}','{7}','{8}')", ma_hangsx, ma_dm, tensp, chitiet, hinhanh,dongia, baohanh,ngaycapnhat,hienthi);
            xulidulieu.thuchienlenh(str);
            Response.Redirect("~/Admin/Sanpham.aspx");
            }
            catch (Exception)
            {
                lbthongbao.Text = "Không thêm được sản phẩm";
            }
        }

    protected void btnnhaplai_Click(object sender, EventArgs e)
    {
        txtbaohanh.Text = "";
        txtdongia.Text = "";
        txttensanpham.Text = "";
        ckethongsokythuat.Text = "";
        txttensanpham.Focus();
    }
}