﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_User_control_Admin_Chitietdonhang : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["user_admin"] != null)
            {
                if (Request.QueryString["ma_kh"] != null)
                {
                    int ma_kh = int.Parse(Request.QueryString["ma_kh"]);
                    string str = "SELECT dathang.sodh, dathang.ngaydathang, dathang.ngaygiaohang, dathang.tennguoinhan, dathang.diachinhan, dathang.hinhthucthanhtoan, dathang.dienthoainhan, dathang.hinhthucgiaohang, dathang.trigia, dathang.dagiao, chitietdathang.soluong, chitietdathang.dongia, chitietdathang.thanhtien, khachhang.hoten FROM dathang INNER JOIN chitietdathang ON dathang.sodh = chitietdathang.sodh INNER JOIN khachhang ON dathang.ma_kh = khachhang.ma_kh where khachhang.ma_kh=" + ma_kh;
                    DataTable dt = xulidulieu.Docbang(str);
                    if (dt.Rows.Count == 0)
                        lbthongbao.Text = "Chưa có đơn đặt hàng nào!";
                    DataList1.DataSource = dt;
                    DataList1.DataBind();
                }
                else
                {
                    lbthongbao.Text = "Bạn không thể xem trang này!";
                }

            }
            else
                Response.Redirect("~/Admin/Dangnhap.aspx");
        }
    }
}