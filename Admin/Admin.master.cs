﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["user_admin"] != null)
        {
            lbtendn.Text = Session["user_admin"].ToString();
        }
    }
    protected void lbtthoat_Click(object sender, EventArgs e)
    {
        Session["user_admin"] = null;
        Response.Redirect("~/Admin/Dangnhap.aspx");
    }
}
