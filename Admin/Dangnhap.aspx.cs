﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;  
using System.Data;
using System.Data.SqlClient;

public partial class Admin_Dangnhap : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["user_admin"] != null)
            {
                Response.Redirect("#");
            }
        //    if (Request.UrlReferrer != null)
        //    {

        //        Session["url"] = Request.UrlReferrer.ToString();
        //    }
        //    else
        //    {
        //        //cái này là người ta đi thẳng vào trang login 
        //        Session["url"] = "Default.aspx"; // hoặc chuyển đến trang khác tùy bạn
        //    }
        }
        txttendangnhap.Focus();
    }
    protected void imbtlogin_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            txtmatkhau.Text = mahoa_md5.Encrypt(txtmatkhau.Text);
            DataTable dt = xulidulieu.Docbang("select * from admin where ten_dn = '" + txttendangnhap.Text.Trim() + "' and matkhau = '" + txtmatkhau.Text.Trim() + "'");
            if (dt.Rows.Count != 0)
            {
                Session["user_admin"] = txttendangnhap.Text;
                Response.Redirect("~/Admin/Default.aspx");
            }
            else
                lbthongbao.Text = "<script>alert('Tên đăng nhập hoặc mật khẩu không đúng')</script>";
        }
        catch (Exception)
        {
            lbthongbao.Text = "<script>alert('Đăng nhập thất bại')</script>";
        }
    }
    
}