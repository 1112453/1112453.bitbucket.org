﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="thanhtoan.ascx.cs" Inherits="UserControl_thanhtoan" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
 <div class="page-title">
    <h1>CHI TIẾT THANH TOÁN</h1>
</div>

<center>
  <div class="fieldset">
       

    <div class="fieldset">
        <h2 class="legend">Thông tin liên hệ</h2>
        <ul class="form-list">
   
    
    <tr>
        <td width="200px" style="font-weight: 700; text-align: right;" >
            Họ tên :</td>
        <td style="font-weight: 700; color: #FF9900">
            <asp:Label ID="lbhoten" runat="server"></asp:Label>
        </td>
    </tr><br />
    <tr>
        <td style="font-weight: 700; text-align: right;" >
            Địa chỉ :</td>
        <td style="font-weight: 700; color: #FF9900">
            <asp:Label ID="lbdiachi" runat="server"></asp:Label>
        </td>
    </tr><br />
    <tr>
        <td style="font-weight: 700; text-align: right;" >
            Điện thoại :</td>
        <td style="font-weight: 700; color: #FF9900">
            <asp:Label ID="lbdienthoai" runat="server"></asp:Label>
        </td>
    </tr><br />
    <tr>
        <td style="font-weight: 700; text-align: right;">
            Email :</td>
        <td style="font-weight: 700; color: #FF9900">
            <asp:Label ID="lbemail" runat="server"></asp:Label>
        </td>
    </tr><br />

    </ul></div>
     <div class="fieldset">
        <h2 class="legend">Thông tin giỏ hàng</h2>
        <ul class="form-list">
    
   
    <tr>
        <td colspan="2" >
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" HorizontalAlign="Justify" 
                 Width="1000px" GridLines="None" style="text-align: center" 
                CellPadding="10" CellSpacing="10" ShowFooter="True">
                <Columns>
                    <asp:BoundField HeaderText="Mã sản phẩm" DataField="masp" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Left" 
                        VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Tên sản phẩm" DataField="tensp" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                        VerticalAlign="Middle" />
                    <ItemStyle Width="400px" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Đơn giá" DataField="dongia" 
                        DataFormatString="{0:0.##}" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                        VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Số lượng">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" BackColor="#EBECEE"
                                style="text-align: right" Width="81px"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtsoluong" runat="server"
                                Text='<%# Eval("soluong") %>' BorderStyle="None"></asp:TextBox>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" 
                                ControlToValidate="txtsoluong" ErrorMessage="Bạn phải nhập số!" ForeColor="Red" 
                                MaximumValue="100" MinimumValue="0" Type="Integer">(*)</asp:RangeValidator>
                        </ItemTemplate>
                        <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" />
                        <ItemStyle Width="100px" />
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Thành Tiền" DataField="thanhtien" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                        VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:ButtonField HeaderText="Tuỳ chọn"
                     Text="Xóa" 
                        CommandName="nutxoa" ButtonType="Button" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                        VerticalAlign="Middle" />
                    </asp:ButtonField>
                </Columns>
                <HeaderStyle BackColor="#0F67A1" />
                <PagerStyle BorderColor="White" />
                <RowStyle BorderColor="White" />
                <SelectedRowStyle BorderColor="#FF9933" />
            </asp:GridView>
        </td>
    </tr><br />
    <tr>
        <td colspan="2" style="text-align: right; font-weight: 700;" >
            Tổng tiền:
            <asp:Label ID="lbtongtien" runat="server" Text="0" ForeColor="Red"></asp:Label>
        &nbsp;VNĐ</td>
    </tr><br />
    </ul></div>


    <div class="fieldset">
        <h2 class="legend">Thông tin giao hàng</h2>
        <ul class="form-list">
    
    <tr>
        <td style="font-weight: 700; text-align: right;" >
            Ngày giao: </td>
        <td>
            <asp:TextBox ID="txtngaygiao" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:TextBox ID="txtngaynhan" runat="server" Visible="true" BorderStyle="None"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                ErrorMessage="Ngày giao hàng phải lớn hơn ngày hiện tại!" 
                ControlToCompare="txtngaynhan" ControlToValidate="txtngaygiao" ForeColor="Red" 
                Operator="LessThanEqual" Type="Date">(*)</asp:CompareValidator>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </asp:ToolkitScriptManager>
            <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtngaygiao" runat="server">
                
</asp:CalendarExtender>
        </td>
    </tr><br />
    <tr>
        <td style="font-weight: 700; text-align: right;" >
            Người nhận: </td>
        <td>
            <asp:TextBox ID="txtnguoinhan" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvnguoinhan" runat="server" 
                ControlToValidate="txtnguoinhan" ErrorMessage="Bạn chưa nhập người nhận.">(*)</asp:RequiredFieldValidator>
        </td>
    </tr><br />
    <tr>
        <td style="font-weight: 700; text-align: right;" >
            Địa chỉ :</td>
        <td>
            <asp:TextBox ID="txtdiachi" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvdiachi" runat="server" 
                ControlToValidate="txtdiachi" ErrorMessage="Bạn chưa nhập địa chỉ.">(*)</asp:RequiredFieldValidator>
        </td>
    </tr><br />
    <tr>
        <td style="font-weight: 700; text-align: right;" >
            Điện thoại :</td>
        <td>
            <asp:TextBox ID="txtdienthoai" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvdienthoai" runat="server" 
                ControlToValidate="txtdienthoai" ErrorMessage="Bạn chưa nhập điện thoại.">(*)</asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" 
                ControlToValidate="txtdienthoai" 
                ErrorMessage="Số điện thoại không đúng định dạng!" 
                MaximumValue="99999999999999" MinimumValue="99999999" Type="Double">(*)</asp:RangeValidator>
        </td>
    </tr><br />
    <tr>
        <td style="font-weight: 700; text-align: right;" >
            Hình thức thanh toán :</td>
        <td style="color: #6666FF;">
            <asp:RadioButton ID="rdtthanhtoantruoc" runat="server" GroupName="thanhtoan" 
                Text="Thanh toán trước khi giao hàng" ForeColor="#FF9900" Checked="True" />
            <br />
            <asp:RadioButton ID="rdtthanhtoansau" runat="server" 
                Text="Thanh toán sau khi giao hàng" GroupName="thanhtoan" 
                ForeColor="#FF9900" />
        </td>
    </tr><br />
    <tr>
        <td style="font-weight: 700; text-align: right;" >
            Hình thức giao hàng :</td>
        <td style="color: #FF6600;">
            <asp:RadioButton ID="rdttructiep" runat="server" GroupName="giaohang" 
                Text="Giao trực tiếp" ForeColor="#3399FF" Checked="True" />
            <br />
            <asp:RadioButton ID="rdtchuyengiao" runat="server" GroupName="giaohang" 
                Text="Chuyển giao" ForeColor="#3399FF" />
        </td>
    </tr><br />
    </ul>
    </div>
     <div class="fieldset">
      
        <ul class="form-list">
    <tr>
        <td colspan="2" style="text-align: center">
            <asp:Button ID="btndongy" runat="server" Height="30px" Text="Đồng ý" 
                Width="120px" onclick="btndongy_Click"/>
        </td>
    </tr><br />
    <tr>
        <td colspan="2" style="text-align: center">
            <asp:Label ID="lbthongbao" runat="server"></asp:Label>
        </td>
    </tr><br />
    <tr>
        <td colspan="2" style="text-align: center">
            <asp:ValidationSummary ID="vsthongbaoloi" runat="server" ShowMessageBox="True" 
                ShowSummary="False" />
        </td>
    </tr><br />
    </ul></div>

    </div></center>

