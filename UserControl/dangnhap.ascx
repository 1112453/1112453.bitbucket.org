﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dangnhap.ascx.cs" Inherits="UserControl_dangnhap" %>
<style type="text/css">
    .style1
    {
        text-align: right;
    }
    .style2
    {
        text-align: left;
    }
</style>
<center>
<div class="page-title">
    <h1>Đăng nhập Alo Homes</h1>
</div>

    <div class="fieldset">
        <h2 class="legend">Thông tin đăng nhập</h2>

        <ul class="form-list">


<table cellpadding="10" cellspacing="10" style="width: 640px;">
    <tr>
        <td style="font-weight: bold" class="style1">
            Tên đăng nhập :</td>

        <td class="style2">
            <asp:TextBox ID="txtdangnhap" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:RegularExpressionValidator ID="rgedn" runat="server" 
                ControlToValidate="txtdangnhap" ErrorMessage="Tên đăng nhập không hợp lệ!" 
                ValidationExpression="^[a-zA-Z'.\s]{1,40}$">(*)</asp:RegularExpressionValidator>
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold" class="style1">
            Mật khẩu :</td>

        <td class="style2">
            <asp:TextBox ID="txtmatkhau" runat="server" Height="30px" Width="200px" 
                TextMode="Password"></asp:TextBox>
        </td>
    </tr>
    <tr>
    <td></td>
        <td colspan="2">
            <asp:Button ID="btndangnhap" runat="server" Height="30px" Text="Đăng nhập" 
                Width="120px" onclick="btndangnhap_Click" />
&nbsp;<asp:Button ID="btnhuy" runat="server" Height="30px" Text="Hủy" Width="120px" 
                onclick="btnhuy_Click" />
        </td>
    </tr><br/>
    <tr>
        <td>
        </td>
        <td style="font-weight: 700">
            <asp:HyperLink ID="hlquenmatkhau" runat="server" ForeColor="#006600" 
                NavigateUrl="~/Quenmatkhau.aspx">Quên mật khẩu?Lấy lại mật khẩu?</asp:HyperLink>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td style="font-weight: 700; color: #0000FF;">
            Bạn chưa có tài khoản?<asp:HyperLink ID="hpldangky" runat="server" 
                Font-Bold="True" ForeColor="Red" NavigateUrl="~/Dangky.aspx">Đăng ký</asp:HyperLink>
        </td>
    </tr>
    <tr>
        <td>
           </td>
        <td>
            <asp:Label ID="lbthongbao" runat="server" ForeColor="#0066FF"></asp:Label>
            <br />
        </td>
    </tr>
</table>

</ul>
</div>
</center>