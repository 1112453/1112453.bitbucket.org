﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="lienhe.ascx.cs" Inherits="UserControl_lienhe" %>





<div id="messages_product_view"><ul class="messages"><li class="success-msg"><ul><li><span>  Nếu bạn có bất cứ thắc mắc, ý kiến đóng góp hoặc câu hỏi gì liên quan đến Alo Home Shops, 
            hãy liên lạc với tôi bằng form dưới đây. Tôi sẽ hồi âm bạn sớm nhất có thể. 
            Tôi luôn cố gắng trả lời tất cả những email gửi tới.</span></li></ul></li></ul></div>
<div class="page-title">
    <h1>Liên hệ Alo Homes</h1>
</div>

    <div class="fieldset">
        <h2 class="legend">Thông tin liên hệ</h2>
        <ul class="form-list">
            <li class="fields">
                <div class="field">
                    <label for="name" class="required"><em>*</em>Họ tên</label>
                    <div class="input-box">
                       <asp:TextBox ID="txtHoten" runat="server" Height="30px" Width="270px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtHoten" ErrorMessage="Bạn chưa nhập họ tên." 
                    ForeColor="Red">(*)</asp:RequiredFieldValidator>   </div>
                </div>
                </li>
                 <li class="fields">

                <div class="field">
                    <label for="name" class="required"><em>*</em>Địa chỉ</label>
                    <div class="input-box">
                        <asp:TextBox ID="txtDiachi" runat="server" Height="30px" Width="270px"></asp:TextBox>  </div>
                </div>
                
                 </li>
                 <li class="fields">
                <div class="field">
                    <label for="name" class="required"><em>*</em>Điện thoại</label>
                    <div class="input-box">
                        <asp:TextBox ID="txtDienthoai" runat="server" Height="30px" Width="270px"></asp:TextBox> </div>
                </div>
                 </li>
                 <li class="fields">
                <div class="field">
                    <label for="name" class="required"><em>*</em>Địa chỉ Email</label>
                    <div class="input-box">
                         <asp:TextBox ID="txtEmail" runat="server" Height="30px" Width="270px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="txtEmail" ErrorMessage="Bạn chưa nhập Email." 
                    ForeColor="Red">(*)</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                    ControlToValidate="txtEmail" ErrorMessage="Định dạng Email chưa đúng." 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    ForeColor="Red">(*)</asp:RegularExpressionValidator>  </div>
                </div>

                 </li>
                
            <li class="wide">
                <label for="comment" class="required"><em>*</em>Nội dung</label>
                <div class="input-box">
                   <asp:TextBox ID="txtNoidung" runat="server" Height="126px" TextMode="MultiLine" 
                    Width="464px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtNoidung" ErrorMessage="Bạn chưa nhập thông điệp." 
                    ForeColor="Red">(*)</asp:RequiredFieldValidator></div>
            </li>
        </ul>
    </div>
    <div class="buttons-set">
         <asp:Label ID="lbthongbao" runat="server"></asp:Label>
          <asp:ValidationSummary ID="vsthongbao" runat="server" 
                    HeaderText="Thông báo lỗi!" ShowMessageBox="True" ShowSummary="False" 
                    style="text-align: center" Font-Bold="True" />
        <asp:Button ID="btnSend" runat="server" Height="31px" onclick="btnSend_Click1" 
                    Text="Gửi Liên Hệ" Width="136px" style="text-align: center" />
    </div>


