﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Mayquayphim.ascx.cs" Inherits="UserControl_Mayquayphim" %>
    <%@ Register assembly="CollectionPager" namespace="SiteUtils" tagprefix="cc1" %>
    <asp:DataList ID="DataList1" runat="server" CellPadding="10" 
    CellSpacing="10" Height="640px" RepeatColumns="3" 
        RepeatDirection="Horizontal" HorizontalAlign="Center">
        <ItemTemplate>
            <table cellpadding="10" cellspacing="10" style="width:100%;">
                <tr>
                    <td style="text-align: center">
                        <asp:Label ID="ten_hangLabel" runat="server" Font-Bold="True" 
                            ForeColor="#FF3300" Text='<%# Eval("ten_sp") %>' />
                    </td>
                </tr>
                <tr>
                        <asp:Image ID="Image1" runat="server" Height="90px" ImageAlign="Middle" 
                            ImageUrl='<%# Eval("hinhminhhoa", "~/images/sanpham/{0}") %>' 
                            style="text-align: center" Width="90px" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; font-weight: bold">
                        Giá :
                        <asp:Label ID="dongiaLabel" runat="server" ForeColor="Blue" 
                            Text='<%# Eval("dongia", "{0:0,00#}") %>' />
                        &nbsp;VNĐ</td>
                </tr>
                <tr>
                    <td style="text-align: center; font-weight: bold;">
                        Ngày cập nhật :<asp:Label ID="ngaycapnhatLabel" runat="server" ForeColor="Blue" 
                            Text='<%# Eval("ngaycapnhat", "{0:dd/MM/yyyy}") %>' />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; font-weight: bold;">
                        <asp:ImageButton ID="imbtchitiet" runat="server" 
                            DescriptionUrl='<%# "~/Chitietsanpham.aspx?masp="+Eval("ma_sp") %>' 
                            ImageUrl="~/images/chitiet.png" 
                            PostBackUrl='<%# "~/Chitietsanpham.aspx?masp="+Eval("ma_sp") %>' />
                        <asp:ImageButton ID="imbtmua" runat="server" ImageUrl="~/images/mua.gif" 
                            DescriptionUrl='<%# "~/Giohang.aspx?Masp="+Eval("ma_sp") %>' 
                            PostBackUrl='<%# "~/Giohang.aspx?Masp="+Eval("ma_sp") %>' />

                    </td>
                </tr>
            </table>
<br />
        </ItemTemplate>
    </asp:DataList>
    <cc1:CollectionPager ID="CollectionPager1" runat="server">
</cc1:CollectionPager>



