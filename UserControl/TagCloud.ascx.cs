﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class UserControl_TagCloud : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TagCoudData();
    }
    private DataTable GetData()
    {
        DataTable dtb = new DataTable();
        //Tạo các Columns
        dtb.Columns.Add("TagName");
        dtb.Columns.Add("Weight");
        dtb.Columns.Add("HrefUrl");
        //Thêm các bản ghi
        dtb.Rows.Add("asp.net", "439", "aspnet.aspx");
        dtb.Rows.Add("SQL", "400", "SQL-server.aspx");
        dtb.Rows.Add("Accessibility", "58", "Accessibility.aspx");
        dtb.Rows.Add("Ajax", "218", "Ajax.aspx");
        dtb.Rows.Add("Csharp", "390", "Csharp.aspx");
        dtb.Rows.Add("OTDH", "250", "OTDH.aspx");
        dtb.Rows.Add("jquery", "175", "jquery.aspx");
        dtb.Rows.Add("Luyện thi", "123", "luyen-thi.aspx");
        dtb.Rows.Add("Store", "89", "Store.aspx");
        dtb.Rows.Add("Facebook", "234", "Facebook.aspx");
        dtb.Rows.Add("Microsoft", "204", "Microsoft.aspx");
        dtb.Rows.Add("google", "290", "google.aspx");
        dtb.Rows.Add("silverlight", "274", "silverlight.aspx");
        dtb.Rows.Add("VTV2", "304", "VTV2.aspx");
        dtb.Rows.Add("Phân trang", "204", "phan-trang.aspx");
        dtb.Rows.Add("Cơ sở dữ liệu", "454", "Co-so-du-lieu.aspx");
        dtb.Rows.Add("Autocomplete", "234", "Autocomplete.aspx");
        return dtb;
    }
    private void TagCoudData()
    {
        WPCumulus.DataSource = GetData();
        WPCumulus.DataBind();
    }
}