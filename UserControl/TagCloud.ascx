﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TagCloud.ascx.cs" Inherits="UserControl_TagCloud" %>
<%@ Register assembly="wpCumulus" namespace="wpCumulus" tagprefix="cc1" %>
<cc1:WPCumulus ID="WPCumulus" runat="server" DataCountField="Weight"  
                        DataTextField="TagName" DataUrlField="HrefUrl" Width="180" Height="220" BorderColor="#DDDDDD" BorderWidth="1" 
                        BackColor="#FFFFFF" HiColor="#66FF33" TagColor1="Red" TagColor2="Blue"  Distr="True"  />