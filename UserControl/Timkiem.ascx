﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Timkiem.ascx.cs" Inherits="UserControl_Timkiem" %>
<table style="width:100%;" cellpadding="10" cellspacing="10" width="640px">
    <tr>
        <td colspan="5" 
            
            
            style="text-align: center; font-weight: 700; color: #FF0000; font-size: x-large">
            TÌM KIẾM THEO GIÁ</td>
    </tr>
    <tr>
        <td>
            Giá từ</td>
        <td>
            <asp:DropDownList ID="ddlgiatu" runat="server">
                <asp:ListItem>2000000</asp:ListItem>
                 <asp:ListItem>5000000</asp:ListItem>
                  <asp:ListItem>10000000</asp:ListItem>
                   <asp:ListItem>20000000</asp:ListItem>
                    <asp:ListItem>50000000</asp:ListItem>
                     <asp:ListItem>10000000</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            Giá đến</td>
        <td>
            <asp:DropDownList ID="ddlgiaden" runat="server">
            <asp:ListItem>2000000</asp:ListItem>
                 <asp:ListItem>5000000</asp:ListItem>
                  <asp:ListItem>10000000</asp:ListItem>
                   <asp:ListItem>20000000</asp:ListItem>
                    <asp:ListItem>50000000</asp:ListItem>
                     <asp:ListItem>10000000</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:Button ID="btntimkiem" runat="server" Height="30px" Text="Tìm kiếm" 
                Width="120px" />
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" BorderColor="White" 
                BorderStyle="None" CellPadding="10" CellSpacing="1" DataKeyNames="ma_sp" 
                DataSourceID="SqlDataSource1" Height="40px" Width="100%">
                <Columns>
                    <asp:BoundField DataField="ma_sp" HeaderText="Mã sản phẩm" 
                        InsertVisible="False" ReadOnly="True" SortExpression="ma_sp">
                    <ControlStyle BorderColor="White" BorderStyle="None" />
                    <HeaderStyle BorderStyle="None" ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ten_sp" HeaderText="Tên sản phẩm" 
                        SortExpression="ten_sp">
                    <HeaderStyle ForeColor="White" Height="40px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ten_hang" HeaderText="Tên hãng sản xuất" 
                        SortExpression="ten_hang">
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ten_dm" HeaderText="Loại sản phẩm" 
                        SortExpression="ten_dm">
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Giá" SortExpression="dongia">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("dongia") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("dongia", "{0:#,##0}") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle ForeColor="White" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="ngaycapnhat" DataFormatString="{0:dd/MM/yyyy}" 
                        HeaderText="Ngày cập nhật" SortExpression="ngaycapnhat">
                    <HeaderStyle ForeColor="White" />
                    </asp:BoundField>
                    <asp:HyperLinkField DataNavigateUrlFields="ma_sp" 
                        DataNavigateUrlFormatString="~/Chitietsanpham.aspx?masp={0}" 
                        HeaderText="Chi tiết" Target="_blank" Text="Chi tiết">
                    <HeaderStyle ForeColor="White" />
                    <ItemStyle ForeColor="White" />
                    </asp:HyperLinkField>
                </Columns>
                <EditRowStyle BorderColor="White" BorderStyle="None" />
                <HeaderStyle BackColor="#0F67A1" />
                <RowStyle BorderColor="White" BorderStyle="None" Height="40px" />
                <SortedAscendingCellStyle BorderStyle="None" ForeColor="White" />
                <SortedAscendingHeaderStyle ForeColor="White" />
                <SortedDescendingCellStyle BorderColor="White" BorderStyle="None" 
                    ForeColor="White" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:vnaovn_shopConnectionString %>" 
                SelectCommand="sanphamtheogia" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlgiatu" Name="Giatu" 
                        PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="ddlgiaden" Name="Giaden" 
                        PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center">
            &nbsp;</td>
        <td style="text-align: center">
            &nbsp;</td>
        <td style="text-align: center">
            &nbsp;</td>
        <td style="text-align: center">
            &nbsp;</td>
    </tr>
</table>

