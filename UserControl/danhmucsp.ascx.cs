﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class UserControl_danhmucsanpham_backup : System.Web.UI.UserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        CollectionPager1.MaxPages = 10000;
        CollectionPager1.PageSize = 9; // số items hiển thị trên một trang.
        CollectionPager1.DataSource = GetDataTable().DefaultView;
        CollectionPager1.BindToControl = dtldanhmucsp;
        dtldanhmucsp.DataSource = CollectionPager1.DataSourcePaged;
        dtldanhmucsp.DataBind();
        
    }
    private DataTable GetDataTable()
    {
        if (Request.QueryString["ma_dm"] == null || Convert.ToInt32(Request.QueryString["ma_dm"]) ==0)
            Response.Redirect("~/Default.aspx");
        int ma_dm = int.Parse(Request.QueryString["ma_dm"].ToString());
        string sql = "select * from sanpham where ma_dm=" + ma_dm;
        DataTable dt = xulidulieu.Docbang(sql);
        if (dt.Rows.Count==0)
            lbthongbao.Text = "Sản phẩm chưa được cập nhật";
        return dt;
    }
}