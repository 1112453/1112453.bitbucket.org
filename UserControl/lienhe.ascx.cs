﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

public partial class UserControl_lienhe : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private void ResetFrom()
    {
        txtHoten.Text = "";
        txtDiachi.Text = "";
        txtDienthoai.Text = "";
        txtEmail.Text = "";
        txtNoidung.Text = "";
    }

    private string MailBody()
    {
        string strHTML = "";
        strHTML += "Họ và tên: " + txtHoten.Text + "<br>";
        strHTML += "Địa chỉ: " + txtDiachi.Text + "<br>";
        strHTML += "Điện thoại: " + txtDienthoai.Text + "<br>";
        strHTML += "Email: " + txtEmail.Text + "<br>";
        strHTML += "Đã gửi qua Form liên hệ với thông điệp:<br><b>";
        strHTML += txtNoidung.Text + "</b>";
        return strHTML;
    }

    protected void btnSend_Click1(object sender, EventArgs e)
    {
        SmtpClient SmtpServer = new SmtpClient();
        SmtpServer.Credentials = new System.Net.NetworkCredential("d.anhkiet78@gmail.com", "pass");
        SmtpServer.Port = 587;
        SmtpServer.Host = "smtp.gmail.com";
        SmtpServer.EnableSsl = true;
        MailMessage mail = new MailMessage();

        try
        {
            mail.From = new MailAddress(txtEmail.Text, txtHoten.Text + " gửi từ form liên hệ", System.Text.Encoding.UTF8);
            mail.To.Add("d.anhkiet78@gmail.com");
            mail.Subject = "Mail từ Form liên hệ ";
            mail.Body = MailBody();
            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            mail.ReplyTo = new MailAddress("d.anhkiet78@gmail.com");
            mail.Priority = MailPriority.High;
            mail.IsBodyHtml = true;
            SmtpServer.Send(mail);
            lbthongbao.Text = "<script>alert('Cảm ơn bạn đã gửi thông điệp đến Alo Home Shops')</script>";
            ResetFrom();
        }
        catch (Exception ex)
        { lbthongbao.Text = ex.Message.ToString(); }
    }
}