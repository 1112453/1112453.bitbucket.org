﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dangky.ascx.cs" Inherits="UserControl_dangky" %>


<center>
<div id="messages_product_view"><ul class="messages"><li class="success-msg"><ul><li><span> Hãy đăng ký thành viên tại Alo Homes để được nhận nhiều phần quà hấp dẫn từ AloHomes.</span></li></ul></li></ul></div>
<div class="page-title">
    <h1>Đăng ký thành viên Alo Homes</h1>
</div>

    <div class="fieldset">
        <h2 class="legend">Thông tin đăng ký</h2>
        <ul class="form-list">
<table cellpadding="10" cellspacing="10" style="width:640px;">
    <tr>
        
    </tr>
    <tr>
        <td style="font-weight: 700">
            Tên đăng nhập :</td>
        <td>
            <asp:TextBox ID="txttendangnhap" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="txttendangnhap" 
                ErrorMessage="Bạn chưa nhập vào tên đăng nhập.">(*)</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td style="font-weight: 700">
            Mật khẩu :</td>
        <td>
            <asp:TextBox ID="txtmatkhau" runat="server" Height="30px" TextMode="Password" 
                Width="200px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="txtmatkhau" ErrorMessage="Bạn chưa nhập mật khẩu.">(*)</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td style="font-weight: 700">
            Nhập lại mật khẩu :</td>
        <td>
            <asp:TextBox ID="txtconfirm" runat="server" Height="30px" TextMode="Password" 
                Width="200px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ControlToValidate="txtconfirm" 
                ErrorMessage="Bạn chưa nhập vào mật khẩu nhập lại.">(*)</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                ControlToCompare="txtmatkhau" ControlToValidate="txtconfirm" 
                ErrorMessage="Mật khẩu nhập lại không trùng khớp.">(*)</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td style="font-weight: 700">
            Email :</td>
        <td>
            <asp:TextBox ID="txtemail" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                ControlToValidate="txtemail" ErrorMessage="Bạn chưa nhập email.">(*)</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtemail" ErrorMessage="Email không đúng định dạng." 
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">(*)</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td style="font-weight: 700">
            Họ tên :</td>
        <td>
            <asp:TextBox ID="txthoten" runat="server" Height="30px" Width="200px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="font-weight: 700">
            Ngày sinh</td>
        <td>
            <asp:DropDownList ID="ddlngay" runat="server" Height="30px" Width="120px">
            </asp:DropDownList>
&nbsp;<asp:DropDownList ID="ddlthang" runat="server" Height="30px" Width="120px">
            </asp:DropDownList>
&nbsp;<asp:DropDownList ID="ddlnam" runat="server" Height="30px" Width="120px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="font-weight: 700">
            Giới tính:</td>
        <td>
            <asp:RadioButton ID="rdtnam" runat="server" GroupName="gioitinh" Text="Nam" 
                Checked="True" />
&nbsp;<asp:RadioButton ID="rdtnu" runat="server" GroupName="gioitinh" Text="Nữ" />
        </td>
    </tr>
    <tr>
        <td style="font-weight: 700">
            Địa chỉ :</td>
        <td>
            <asp:TextBox ID="txtdiachi" runat="server" Height="72px" TextMode="MultiLine" 
                Width="421px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="font-weight: 700">
            Điện thoại :</td>
        <td>
            <asp:TextBox ID="txtdienthoai" runat="server" Height="30px" Width="200px"></asp:TextBox>
            <asp:RangeValidator ID="RangeValidator1" runat="server" 
                ControlToValidate="txtdienthoai" 
                ErrorMessage="Số điện thoại không đúng định dạng!" 
                MaximumValue="99999999999999" MinimumValue="99999999" Type="Double">(*)</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td style="text-align: center">
            <asp:Button ID="btndangky" runat="server" Height="30px" Text="Đăng Ký" 
                Width="100px" onclick="btndangky_Click" />
&nbsp;<asp:Button ID="btnnhaplai" runat="server" Height="30px" Text="Nhập lại" 
                Width="100px" onclick="btnnhaplai_Click" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td style="text-align: center">
            <asp:Label ID="lbthongbao" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            </td>
        <td style="text-align: center" class="style5">
            <asp:ValidationSummary ID="vsthongbaoloi" runat="server" 
                HeaderText="Thông báo lỗi." ShowMessageBox="True" ShowSummary="False" />
        </td>
    </tr>
</table>
</ul>
</div>
</center>

