﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;

public partial class UserControl_giohang : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["tendangnhap"] == null)
            {
                Response.Redirect("~/Dangnhap.aspx");
            }
            if (Request.QueryString["Masp"] != null)
            {
                int masp = int.Parse(Request.QueryString["Masp"]);
                DataTable dt = xulidulieu.Docbang("select ten_sp,dongia from sanpham where ma_sp="+masp);
                string tensp = dt.Rows[0][0].ToString();
                int dongia=int.Parse(dt.Rows[0][1].ToString());
                int soluong=1;
                Themvaogiohang(masp,tensp,dongia,soluong);
            }
            if(Session["giohang"]!=null)
            {
                DataTable dt=new DataTable();
                dt=(DataTable)Session["giohang"];
                System.Decimal tongthanhtien=0;
                foreach(DataRow row in dt.Rows)
                {
                    row["thanhtien"]=Convert.ToInt32(row["soluong"])*Convert.ToDecimal(row["dongia"]);
                    tongthanhtien+=Convert.ToDecimal(row["thanhtien"]);
                    lbtongcong.Text=tongthanhtien.ToString();
                }
                GridView1.DataSource=dt;
                GridView1.DataBind();
            }
        }
    }
    public void Themvaogiohang(int masp, string tensp, int dongia, int soluong)
    {
        DataTable dt;
        if (Session["giohang"] == null)
        {
            dt = new DataTable();
            dt.Columns.Add("masp");
            dt.Columns.Add("tensp");
            dt.Columns.Add("dongia");
            dt.Columns.Add("soluong");
            dt.Columns.Add("thanhtien");
        }
        else
            dt = (DataTable)Session["giohang"];
        int dong = spdacotronggiohang(masp, dt);
        if (dong != -1)
        {
            dt.Rows[dong]["soluong"] = Convert.ToInt32(dt.Rows[dong]["soluong"]) + soluong;
        }
        else
        {
            DataRow dr = dt.NewRow();
            dr["masp"] = masp;
            dr["tensp"] = tensp;
            dr["dongia"] = dongia;
            dr["soluong"] = soluong;
            dr["thanhtien"] = soluong * dongia;
            dt.Rows.Add(dr);
        }
        Session["giohang"] = dt;
    }
    public static int spdacotronggiohang(int masp, DataTable dt)
    {
        int dong = -1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (int.Parse(dt.Rows[i]["masp"].ToString()) == masp)
            {
                dong = i;
                break;
            }
        }
        return dong;
    }
    protected void btnthanhtoan_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Thanhtoan.aspx");
    }
    protected void btncapnhat_Click(object sender, EventArgs e)
    {
        DataTable dt = (DataTable)Session["giohang"];
        foreach (GridViewRow r in GridView1.Rows)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToString(GridView1.DataKeys[r.DataItemIndex].Value) == dr["masp"].ToString())
                {
                    TextBox t = (TextBox)r.Cells[2].FindControl("txtsoluong");
                    if (Convert.ToInt32(t.Text) <= 0)
                        dt.Rows.Remove(dr);
                    else
                        dr["soluong"] = t.Text;
                    break;
                }
            }
        }
        Session["giohang"] = dt;
        Response.Redirect("~/Giohang.aspx");

    }
    protected void btntieptuc_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx");
    }
    protected void btnxoagiohang_Click(object sender, EventArgs e)
    {
        Session["giohang"] = null;
        Response.Redirect("~/Default.aspx");
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName =="nutxoa")
        {
            int chiso = int.Parse(e.CommandArgument.ToString());
            try
            {
                DataTable dt = (DataTable)Session["giohang"];
                dt.Rows.RemoveAt(chiso);
                Session["giohang"] = dt;
                Response.Redirect("~/Giohang.aspx");
            }
            catch
            {
                Response.Redirect("~/Giohang.aspx");
            }
        }
    }

    public static int Masp { get; set; }
}