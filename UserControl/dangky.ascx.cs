﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
public partial class UserControl_dangky : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            loadngaythangnam();
    }
    private void loadngaythangnam()
    {
        if (!IsPostBack)
        {
             if (Session["tendangnhap"] != null)
                {
                    Response.Redirect("#");
                }
            ddlnam.DataSource = Enumerable.Range(1975, (DateTime.Now.Year - 1975) + 1);
            ddlnam.DataBind();
            ddlthang.DataSource = Enumerable.Range(1,12);
            ddlthang.DataBind();
            ddlngay.DataSource = Enumerable.Range(1, 31);
            ddlngay.DataBind();
        }
    }
   
    protected void btndangky_Click(object sender, EventArgs e)
    {
        try
        {
            txtmatkhau.Text = mahoa_md5.Encrypt(txtmatkhau.Text);
            string tendn = txttendangnhap.Text;
            string matkhau = txtmatkhau.Text;
            string hoten = txthoten.Text;
            string diachi = txtdiachi.Text;
            string dienthoai = txtdienthoai.Text;
            string gioitinh;
            if (rdtnam.Checked == true)
                gioitinh = "true";
            else
                gioitinh = "false";
            string email = txtemail.Text;
            string ngaysinh = ddlngay.SelectedItem.ToString() + '/' + ddlthang.SelectedItem.ToString() + '/' + ddlnam.SelectedItem.ToString();
            string str = string.Format("insert into khachhang(ten_dn,matkhau,email,hoten,ngaysinh,gioitinh,diachi,dienthoai) values ('{0}',N'{1}','{2}',N'{3}','{4}','{5}',N'{6}','{7}')",tendn, matkhau,email,hoten,ngaysinh,gioitinh,diachi,dienthoai) ;
            xulidulieu.thuchienlenh(str);
            Session["tendangnhap"] = tendn;
            Response.Redirect("~/Default.aspx");
        }
        catch (Exception)
        {
            lbthongbao.Text = "Đăng ký thất bại";
        }

    }
    protected void btnnhaplai_Click(object sender, EventArgs e)
    {
        txttendangnhap.Text = "";
        txtdiachi.Text = "";
        txtemail.Text = "";
        txtmatkhau.Text = "";
        txtdienthoai.Text = "";
        txtconfirm.Text = "";
        txthoten.Text = "";
        ddlngay.SelectedIndex = 0;
        ddlthang.SelectedIndex = 0;
        ddlnam.SelectedIndex = 0;
    }

    public string input { get; set; }
}