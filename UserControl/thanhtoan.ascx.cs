﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class UserControl_thanhtoan : System.Web.UI.UserControl
{
    int makh;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtngaygiao.Text = DateTime.Now.ToShortDateString() ;
        }
        if (Session["tendangnhap"] == null)
            Response.Redirect("~/Dangnhap.aspx");
        if (Session["giohang"] == null)
            Response.Redirect("~/giohang.aspx");
        if (Session["tendangnhap"] != null)
        {
            string s="Select ma_kh,hoten,diachi,dienthoai,email from khachhang where ten_dn='" +Session["tendangnhap"].ToString() + "'";
            DataTable dt = xulidulieu.Docbang(s);
            if (dt.Rows.Count != 0)
            {
                makh = int.Parse(dt.Rows[0][0].ToString());
                lbhoten.Text = dt.Rows[0][1].ToString();
                lbdiachi.Text = dt.Rows[0][2].ToString();
                lbdienthoai.Text = dt.Rows[0][3].ToString();
                lbemail.Text = dt.Rows[0][4].ToString();
            }
        }
        if (Session["giohang"] != null)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["giohang"];
            System.Int32 tongThanhTien = 0;
            foreach (DataRow r in dt.Rows)
            {
                r["Thanhtien"] = Convert.ToInt32(r["SoLuong"]) *
                Convert.ToInt32(r["Dongia"]);
                tongThanhTien += Convert.ToInt32(r["Thanhtien"]);
                lbtongtien.Text = tongThanhTien.ToString();
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void btndongy_Click(object sender, EventArgs e)
    {
        int httt, htgh;
        String Ngaygiao, Ngaydathang, Tennguoinhan, Diachinhan, Dienthoainhan;
        Ngaydathang =DateTime.Today.ToString("MM/dd/yyyy");
        Ngaygiao = txtngaygiao.Text;
        Tennguoinhan = txtnguoinhan.Text;
        Diachinhan = txtdiachi.Text;
        Dienthoainhan = txtdienthoai.Text;
        Int32 tongThanhTien = Int32.Parse(lbtongtien.Text);
        if (rdtthanhtoantruoc.Checked == true)
            httt = 1;
        else
            httt = 0;
        if (rdttructiep.Checked == true)
            htgh = 1;
        else
            htgh = 0;

        try
        {
            string s = string.Format("insert into dathang(ma_kh,ngaydathang,ngaygiaohang,tennguoinhan,diachinhan,dienthoainhan,hinhthucthanhtoan,hinhthucgiaohang,trigia) values ('{0}','{1}','{2}',N'{3}',N'{4}','{5}',N'{6}','{7}','{8}')", makh, Ngaydathang, Ngaygiao,Tennguoinhan,Diachinhan, Dienthoainhan, httt, htgh,tongThanhTien);
            xulidulieu.thuchienlenh(s);
            s = "Select Max(sodh) from dathang Where ma_kh=" + makh;
            int SoDH = int.Parse(xulidulieu.getdata(s).ToString());
            DataTable dt = new DataTable();
            dt = (DataTable)Session["giohang"];
            int Masp, Soluong, Dongia, Thanhtien;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Masp = int.Parse(dt.Rows[i]["masp"].ToString());
                Soluong = int.Parse(dt.Rows[i]["soluong"].ToString());
                Dongia = int.Parse(dt.Rows[i]["dongia"].ToString());
                Thanhtien = int.Parse(dt.Rows[i]["thanhtien"].ToString());
                s = "INSERT INTO chitietdathang(sodh,ma_sp, soluong,dongia,thanhtien) VALUES(" + SoDH + "," + Masp + "," + Soluong + "," + Dongia + "," + Thanhtien + ")";
                xulidulieu.thuchienlenh(s);
            }
            Response.Redirect("~/Xacnhandonhang.aspx");
        }
        catch(Exception)
        {
            lbthongbao.Text = " Xin cáo lỗi? <br> Quá trình cập nhật dữ liệu không thành công!";
        }
    }
}