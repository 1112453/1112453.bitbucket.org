﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="giohang.ascx.cs" Inherits="UserControl_giohang" %>

<center>
<div class="page-title">
    <h1>THÔNG TIN GIỎ HÀNG</h1>
</div>

    <div class="fieldset">
        <h2 class="legend">Chi tiết giỏ hàng</h2>

        <ul class="form-list">
<table style="width:100%" align="center">
    <tr>
        <br />
    </tr>
    <tr>
        <td style="text-align: center">
        <center>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" HorizontalAlign="Center" 
                onrowcommand="GridView1_RowCommand" DataKeyNames="Masp" Width="1000px" 
                BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" 
                CellPadding="3" CellSpacing="1" GridLines="None">
                <Columns>
                    <asp:BoundField HeaderText="Mã sản phẩm" DataField="masp" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Left" 
                        VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Tên sản phẩm" DataField="tensp" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                        VerticalAlign="Middle" />
                    <ItemStyle Width="400px" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Đơn giá" DataField="dongia" 
                        DataFormatString="{0:0.##}" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                        VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Số lượng">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" BackColor="#EBECEE"
                                style="text-align: right" Width="81px"></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:TextBox ID="txtsoluong" runat="server"
                                Text='<%# Eval("soluong") %>' BorderStyle="None"></asp:TextBox>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" 
                                ControlToValidate="txtsoluong" ErrorMessage="Bạn phải nhập số!" ForeColor="Red" 
                                MaximumValue="100" MinimumValue="0" Type="Integer">(*)</asp:RangeValidator>
                        </ItemTemplate>
                        <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                            VerticalAlign="Middle" />
                        <ItemStyle Width="100px" />
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Thành Tiền" DataField="thanhtien" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                        VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:ButtonField HeaderText="Tuỳ chọn"
                     Text="Xóa" 
                        CommandName="nutxoa" ButtonType="Button" >
                    <HeaderStyle ForeColor="White" Height="40px" HorizontalAlign="Center" 
                        VerticalAlign="Middle" />
                    </asp:ButtonField>
                </Columns>
                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                <PagerStyle BorderColor="White" BackColor="#C6C3C6" ForeColor="Black" 
                    HorizontalAlign="Right" />
                <RowStyle BorderColor="White" ForeColor="Black" BackColor="#DEDFDE" />
                <SelectedRowStyle BorderColor="#FF9933" BackColor="#9471DE" Font-Bold="True" 
                    ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#594B9C" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#33276A" />
            </asp:GridView>
            </center>
        </td>
    </tr>
    <tr>
        <td><center>
            Tổng cộng :
       <asp:Label ID="lbtongcong" runat="server" Text="0"></asp:Label>
        &nbsp;<strong>VNĐ</strong></center></td>
    </tr>
    <tr>
       
        <td><center>
            <asp:Button ID="btntieptuc" runat="server" Height="30px" 
                onclick="btntieptuc_Click" Text="Tiếp tục mua hàng" Width="150px" />
        
            <asp:Button ID="btnxoagiohang" runat="server" Height="30px" 
                onclick="btnxoagiohang_Click" Text="Xóa giỏ hàng" Width="120px" />
        
            <asp:Button ID="btncapnhat" runat="server" Height="30px" 
                onclick="btncapnhat_Click" Text="Cập nhật" Width="120px" />
        
            <asp:Button ID="btnthanhtoan" runat="server" Height="30px" 
                onclick="btnthanhtoan_Click" Text="Thanh toán" Width="120px" />
        
       </center></td> 
    </tr>
    <tr>
        <td colspan="6">
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                ShowMessageBox="True" ShowSummary="False" />
        </td>
    </tr>
</table>

</ul>
</div></center>