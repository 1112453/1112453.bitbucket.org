﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class UserControl_dangnhap : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["tendangnhap"] != null)
            {
                Response.Redirect("#");
            }
        }
        txtdangnhap.Focus();
    }
    protected void btndangnhap_Click(object sender, EventArgs e)
    {
      
        try
        {
            txtmatkhau.Text = mahoa_md5.Encrypt(txtmatkhau.Text);
            DataTable dt = xulidulieu.Docbang("select * from khachhang where ten_dn = '" + txtdangnhap.Text.Trim() + "' and matkhau = '" + txtmatkhau.Text.Trim() + "'");
            if (dt.Rows.Count != 0)
            {
                Session["tendangnhap"] = txtdangnhap.Text;
                Response.Redirect("~/Default.aspx");
            }
            else
                lbthongbao.Text = "<script>alert('Tên đăng nhập hoặc mật khẩu không đúng')</script>";
        }
        catch (Exception)
        {
            lbthongbao.Text = "<script>alert('Đăng nhập thất bại')</script>";
        }
    }
    protected void btnhuy_Click(object sender, EventArgs e)
    {
        txtdangnhap.Text = "";
        txtdangnhap.Focus();
        txtmatkhau.Text = "";
    }
}