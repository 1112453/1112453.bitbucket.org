﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for xulidulieu
/// </summary>
public class xulidulieu
{
    static string strcnn = ConfigurationManager.ConnectionStrings["vnaovn_shopConnectionString"].ConnectionString.ToString();
	public static DataTable Docbang(string lenhsql)
	{
        SqlConnection conn = new SqlConnection(strcnn);
        SqlDataAdapter bodocghi = new SqlDataAdapter(lenhsql, conn);
        DataTable bang = new DataTable();
        bodocghi.Fill(bang);
        return bang;
	}
    public static void thuchienlenh(string lenhsql)
    {
       using (SqlConnection conn = new SqlConnection(strcnn))
    {
        conn.Open();
        SqlCommand bolenh = new SqlCommand(lenhsql, conn);
        bolenh.ExecuteNonQuery();
        conn.Close();
}

    }
    public static string getdata(string lenhsql)
    {
        using (SqlConnection cnn = new SqlConnection(strcnn))
        {
            cnn.Open();
            SqlCommand cmd = cnn.CreateCommand();
            cmd.CommandText = lenhsql;
            string result = "" + cmd.ExecuteScalar().ToString();
            cnn.Close();
            return result;
        }
    }
}